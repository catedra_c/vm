# -*- mode: ruby -*-
# vi: set ft=ruby :

# Para generar una imagen virtual de 32 bits, se debe especificar la variable de
# ambiente de este proceso `ARCH` con valor `32`. En caso contrario, se utilizará
# una arquitectura base de 64 bits para la imagen virtual. Por ejemplo, los
# siguientes comandos crean imágenes de 32 y 64 bits, respectivamente:
# $ ARCH=32 vagrant up
# Por defecto, se usará la arquitectura del sistema base.
arch_32b = ENV.fetch("ARCH") { 1.size == 8 ? 64 : 32 }.to_i == 32
bits = arch_32b ? 32 : 64

# Configuración para la imagen virtual para Seminario de Lenguajes - Opción C.
Vagrant.configure("2") do |config|
  # A continuación se presenta la configuración recomendada por la cátedra para
  # la creación y configuración de la máquina virtual. Estos valores son generales,
  # y pueden ser adaptados acorde al sistema host donde se va a ejecutar la máquina
  # virtual. La documentación completa de Vagrant y las opciones disponibles para
  # este archivo pueden consultarse en: https://docs.vagrantup.com.

  # La imagen base ("box") para la máquina virtual es Ubuntu 16.04 de 32 bits.
  # Se pueden consultar todas las imágenes base disponibles en:
  # https://atlas.hashicorp.com/search.
  #
  # En caso de necesitar usar una arquitectura de 32 bits, invocar `vagrant up` de
  # la siguiente manera:
  # $ ARCH=32 vagrant up
  config.vm.box = arch_32b ? "bento/ubuntu-16.04-i386" : "bento/ubuntu-16.04"

  # Crea una red privada entre el sistema host (equipo físico) y la máquina virtual
  # para permitir acceder a ésta última.
  config.vm.network "private_network", ip: "10.100.10.10"

  # Crea un directorio compartido entre el host y la máquina virtual para poder
  # acceder al código desarrollado desde ambos sistemas.
  # Si se desea, se puede cambiar la ruta del directorio en el host para compartir
  # otro, modificando el valor "./codigo" por una ruta absoluta o relativa al
  # directorio donde se encuentra este archivo Vagrantfile.
  # Para mayor información sobre qué es una ruta relativa o absoluta, puede
  # consultarse este artículo de Wikipedia:
  # https://es.wikipedia.org/wiki/Ruta_(inform%C3%A1tica).
  # Desde la máquina virtual, la carpeta compartida será accesible desde el
  # directorio /codigo.
  config.vm.synced_folder "./codigo", "/codigo"

  # Define el nombre interno de la VM para el provider (VirtualBox).
  config.vm.define("seminario_c_#{bits}") { |m| }

  # Esta es la configuración de la máquina virtual que se creará en VirtualBox.
  # Nos permite modificar el hardware virtual y las características de la máquina.
  # Acá es donde se puede llegar a necesitar ajustar algún valor, dependiendo
  # del hardware físico sobre el cual correrá la máquina virtual.
  config.vm.provider "virtualbox" do |vb|
    # Nombre que se le dará a la máquina virtual en VirtualBox.
    # Si se desea crear más de una máquina virtual con este Vagrantfile, se debe
    # cambiar este atributo de la máquina para diferenciarlas.
    vb.name = "Seminario C (#{bits} bits)"

    # Cantidad de memoria (en MiB) para la máquina virtual
    vb.memory = "256"
  end

  # Esta sección especifica un script de shell para preparar la máquina virtual
  # para su uso. Lo que hace es instalar las dependencias necesarias para poder
  # desarrollar en el lenguaje C, apuntando a cumplir con todos los requerimientos
  # que puedan surgir a lo largo de la materia.
  config.vm.provision "shell", inline: <<-SHELL
    apt-get update -qq
    # Opcionalmente, se puede descomentar la linea siguiente para actualizar el sistema:
    # apt-get upgrade -y
    apt-get -y install build-essential \
                       gdb \
                       valgrind \
                       cppcheck \
                       splint \
                       vim \
                       emacs \
                       mcedit \
                       manpages-dev
    if [ $(grep -ce '^cd /codigo$' /home/vagrant/.bashrc) -eq 0 ]; then
      echo "cd /codigo" >> /home/vagrant/.bashrc
    fi
  SHELL
end
