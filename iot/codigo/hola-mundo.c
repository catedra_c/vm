#include <stdio.h>

// Este es un script de prueba para verificar el correcto funcionamiento
// de todas las dependencias para compilar programas en C desde la máquina
// virtual.
//
// Para compilar este programa, se puede utilizar el siguiente comando:
//   gcc -Wall -o hola.bin hola-mundo.c
// Y una vez compilado, se puede probar el binario generado mediante:
//   ./hola.bin
//
// Si al ejecutar el binario ves "Hola mundo!" en la terminal, todo está
// funcionando bien :)

int main() {
  printf("Hola mundo!\n");
}
