# Máquina virtual para la cursada

Este proyecto provee lo necesario para generar una máquina virtual preparada
con todas las dependencias necesarias para desarrollar, compilar, ejecutar y
utilizar los temas comprendidos en la cursada de la materia Seminario de
Lenguajes - opción C, de la Facultad de Informática de la UNLP.

## Objetivo

Este proyecto apunta a simplificar el proceso necesario para poder tener un
ambiente de desarrollo para el aprendizaje y la utilización del
lenguaje C, mediante la facilitación de un ambiente normalizado para utilizar en
el desarrollo, la compilación y la ejecución de programas escritos en ese
lenguaje.

Mediante el uso de esta máquina virtual, no será necesario lidiar con
dependencias y/o instalaciones en las distintas plataformas para comenzar a
desarrollar en C, ya que la máquina virtual viene provista de todo lo necesario
para tal fin.

## Requisitos previos

Para poder generar la máquina virtual, se debe contar con las siguientes
dependencias instaladas y funcionando:

* [VirtualBox](https://www.virtualbox.org/wiki/Downloads) versión 5.1 o posterior.
* [Vagrant](https://www.vagrantup.com/downloads.html) versión 1.9.2 o posterior.
* (Sólo en Windows) [Git](https://git-scm.com/download/win) 2.x.

Los sitios de estos productos poseen guías detalladas para su instalación y
configuración según la plataforma en la cual se los ejecutará, es por esto que
recomendamos referirse a la documentación de cada uno para realizar las
correspondientes instalaciones.

Durante la instalación de Git debe tenerse una consideración especial en la
etapa "Adjusting your PATH environment". En este paso debe elegirse la opción
"Use Git and optional Unix tools from the Windows Command Prompt".

Una vez se disponga de este software instalado, puede continuarse con la
siguiente sección ("Preparación de la máquina virtual").

## Terminología / Conceptos importantes

Definiremos algunos términos y elementos importantes de este documento para
evitar ambigüedades:

* **Máquina virtual:** software que emula ser un equipo real (o *físico*) en el
  cual se puede ejecutar un Sistema Operativo completo, tal como si se tratase
  de un equipo físico. El Sistema Operativo que se ejecuta en la máquina virtual
  se denomina *guest*, mientras que el Sistema Operativo en que se ejecuta la
  máquina virtual se denomina *host*. Una de las ventajas que ofrece el uso de
  máquinas virtuales es la separación del Sistema Operativo *guest* con respecto
  del *host*, y la posibilidad de ejecutar diferentes Sistemas Operativos
  simultáneamente sobre el mismo hardware, como por ejemplo ejecutar un Sistema
  Operativo GNU/Linux de *guest* sobre un Microsoft Windows de *host*, o
  viceversa.
* **Ejecutar comandos:** escribir (o copiar y pegar) comandos en una consola
  interactiva (o terminal, o línea de comandos). Cuando se hace referencia a que
  la ejecución debe realizar estando situado en un directorio particular, quiere
  decir que se debe ejecutar el comando `cd` con la ruta al directorio indicado
  **antes** de ejecutar el comando, de manera tal que los comandos subsiguientes
  al cambio de directorio (`cd`) se ejecuten en el contexto del directorio
  correcto.
* **Virtualizador:** Es el software propiamente dicho que se encarga de ejecutar
  la máquina virtual sobre el *host*. En nuestro caso, el virtualizador es
  VirtualBox.
* **Imagen de máquina virtual:** pueden asemejarse a "discos preinstalados" con
  un sistema operativo base. Se las utiliza para crear, a partir de éstos, las
  máquinas virtuales con un Sistema Operativo que tenga las características
  deseadas.
* **Tareas de provisión:** serie de pasos necesarios para proveer de las
  características (software, restricciones de hardware, etc) deseadas a una
  máquina virtual.
* **`Vagrantfile`:** es el archivo de configuración de Vagrant, en el cual se
  define el plan de trabajo a realizar para provisionar la máquina virtual.
  Es un archivo de configuración escrito en el lenguaje Ruby, que se organiza en
  claves de configuración.

## Preparación de la máquina virtual

La preparación de la máquina virtual (así como el resto de su ciclo de vida) se
hace utilizando la herramienta Vagrant, la cual se encarga de crear la máquina
virtual en VirtualBox a partir de una imagen de máquina virtual base, iniciar la
máquina virtual, configurarla, ejecutar las tareas de provisión sobre ella, y
permitir el acceso a la misma, una vez esté funcionando.

Para crear por primera vez o iniciar (una vez creada) la máquina virtual,
se debe ejecutar el comando presentado a continuación desde una terminal.

```console
vagrant up
```

> **Nota:** Es importante que este comando sea ejecutado estando situado en el
mismo directorio que se encuentra el archivo `Vagrantfile`.

Luego que este comando termine (que dependiendo de la velocidad de conexión a
internet puede ser bastante tiempo), y si todo funcionó correctamente, ya
tendremos disponible la máquina virtual para su uso. Esto puede chequearse desde
la interfaz gráfica de VirtualBox (donde debiera aparecer una máquina virtual
con el nombre "Seminario C"), o bien siguiendo los pasos de utilización de la
máquina virtual, tal como se detallen en la siguiente sección.

### Algunas salvedades

* Si el sistema *host* es de 32 bits, deberá especificarse que se utilice una
  arquitectura base de 32 bits en la máquina virtual. Esto se hará automáticamente
  con cada comando `vagrant` que ejecutemos utilizando el `Vagrantfile` provisto,
  pero puede forzarse definiendo la variable de ambiente `ARCH` y asignándole el
  valor `32`, como se muestra a continuación:

  ```
  export ARCH=32
  vagrant up
  ```

> **Nota:** si se ha forzado la generación de una arquitectura distinta a la del
> sistema host, se deberá tener definida la variabla de ambiente `ARCH` con el
> mismo valor cada vez que se vaya a ejecutar un comando `vagrant` relacionado a
> la máquina virtual creada con la arquitectura distinta.

* Si el sistema *host* no posee grandes recursos de hardware, puede modificarse
  el archivo `Vagrantfile` para reducir los recursos requeridos para la máquina
  virtual. Esto se puede hacer modificando el valor de configuración
  `vb.memory`.

> **Nota:** en caso de modificar el archivo `Vagrantfile`, se deberá realizar
> una actualización de la máquina virtual, tal como se detalle en la sección
> "Actualización de la máquina virtual", para que los cambios sean efectivos.

## Utilización de la máquina virtual

Una vez que se tenga funcionando la máquina virtual, resta ingresar a la misma
mediante el siguiente comando, que debe ejecutarse también situado en el
directorio donde se encuentra el archivo `Vagrantfile`:

```console
vagrant ssh
```

Esto nos dará un acceso mediante consola a la máquina virtual, momento a partir
del cual todo comando que ejecutemos se estará ejecutando **en la máquina
virtual**, en el Sistema Operativo *guest*.

> **Recordar:** si se ha forzado la generación de una arquitectura distinta a la del
> sistema host, este comando deberá ejecutarse con la variable de ambiente `ARCH`
> definida con el mismo valor (`32` ó `64`) que se creó la máquina virtual.

Para probar si todo funciona como se espera, se puede ejecutar el comando `ls`
para ver el contenido del directorio `codigo`, que es donde se encontrará
situado al ingresar mediante `vagrant ssh`. La primera vez que se ejecute esto,
se debería ver el archivo `hello-world.c` en ese directorio:

```console
vagrant@vagrant:/codigo$ ls
hola-mundo.c
```

Luego, podemos compilar y ejecutar ese programa para corroborar que efectivamente
todo funciona como se espera:

```console
vagrant@vagrant:/codigo$ gcc -Wall -o hola.bin hola-mundo.c
vagrant@vagrant:/codigo$ ./hola.bin
Hola mundo!
```

Si la salida del último comando es `Hola mundo!`, quiere decir que todo funcionó bien.

## Actualización de la máquina virtual

En caso de haber modificado el archivo `Vagrantfile`, se debe ejecutar el
siguiente comando situado en el mismo directorio que dicho archivo:

```console
vagrant provision
```

> **Recordar:** si se ha forzado la generación de una arquitectura distinta a la del
> sistema host, este comando deberá ejecutarse con la variable de ambiente `ARCH`
> definida con el mismo valor (`32` ó `64`) que se creó la máquina virtual.

Esto ejecutará nuevamente las tareas de provisión de la máquina virtual para
reflejar los cambios realizados.

## Borrado de la máquina virtual

Si se desea borrar la máquina virtual que se ha generado en VirtualBox, basta
con ejecutar el siguiente comando estando situado en el mismo directorio donde
está el archivo `Vagrantfile`:

```console
vagrant destroy -f
```

> **¡Advertencia!** Este comando borrará la máquina virtual junto con su sistema
> de archivos, lo cual puede generar pérdida de datos que no estén guardados en
> el directorio compartido `codigo` antes mencionada. Los archivos que estén en el
> directorio compartido `codigo` (o en un subdirectorio de éste), no serán
> borrados del equipo *host*.

> **Recordar:** si se ha forzado la generación de una arquitectura distinta a la del
> sistema host, este comando deberá ejecutarse con la variable de ambiente `ARCH`
> definida con el mismo valor (`32` ó `64`) que se creó la máquina virtual.

## Licencia

Este contenido y todo lo comprendido en este proyecto se encuentra distribuido
bajo la licencia [Creative Commons Atribución 2.5 Argentina (CC BY 2.5 AR)](https://creativecommons.org/licenses/by/2.5/ar/).
